# Duo

The goal of this section is to familiarise yourself with a couple of exciting GitLab Duo features by going through a simple example workflow where we work through a serious security vulnerability that was assigned to us.

## Setup Duo Chat
Throughout this workshop you will be playing around with different features and functionality of GitLab and sometimes you might get stuck. Luckily GitLab Duo Chat is here to help! Let's make sure you know where to find it and how to use it:



- [ ] Look for the `help` option on the bottom left of your screen.
- [ ] Click on it and select the `GitLab Duo Chat` option within the pop up
- [ ] Now you have access to the GitLab Chatbot! Just enter your question in the text box and press the `send` button or press the `Enter` button on your keyboard


Feel free to ask it any questions you might have throughout today's workshop. GitLab Duo Chat will keep track of the context of your current conversation. you can reset this by simply typing the `/reset` quick command. 

## 
